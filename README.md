# debug-tools

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: kgol-debug
  namespace: kgol
spec:
  containers:
  - name: kgol
    image: registry.gitlab.com/k-gol-platform/debug-tools:latest
    imagePullPolicy: IfNotPresent
    command:
      - sleep
      - "3600"
```

```bash
kubectl create -f kgol-pod.yaml
kubectl exec -n kgol -it kgol-debug -- /bin/bash
```
